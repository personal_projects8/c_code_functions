/**
 * @file arrays.c
 * This is the source code for my personal arrays function
 */

#include "arrays.h"
#include <stdlib.h>

int_array *int_createArray(){
    int_array *array = malloc(sizeof(int_array));
    if(array != NULL){
        int_array_node *head = malloc(sizeof(int_array_node));
        if(head == NULL){
            int_array *deletion = array;
            array = NULL;
            free(deletion);
        } else {
            head->number = 0; ///<setting the first node to 0 rather than a randum number
            head->successor = NULL; ///<There is nothing at the tail
            int_goToHead(array);
            array->size = 1;
        }
    }
    return array;
}

void int_deleteArray(int_array *array){
    int_goToHead(array);
    int_array_node *toBeDeleted;
    while(array->size > 0){
        toBeDeleted = array->current;
        int_goToNext(array);
        free(toBeDeleted);
        array->size -= 1;
    }
    free(array);
}

array_errorCode addNode(int_array *array, int number){
    array_errorCode rValue = success;
    int_array_node *newNode = malloc(sizeof(int_array_node));
    if(newNode == NULL){
        rValue = noMemory;
    }else{
        newNode->number = number;
        newNode->successor = array->current->successor;
        array->current->successor = newNode;
    }
    return rValue;
}

void int_goToHead(int_array *array){
    array->current = array->head;
}

array_errorCode int_goToNext(int_array *array){
    array_errorCode rValue = success;
    if(array->current->successor == NULL){
        rValue = illegalNode;
    } else {
        array->current = array->current->successor;
    }
    return rValue;
}