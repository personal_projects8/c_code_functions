/**
 * @file arrays.h
 * Header file for my personal arrays functions
 *
 * @author Eoghan Conlon
 *
 * @date Created: 8/12/2022 Last Modified: 8/12/2022
 */

#ifndef CS4023_ARRAYS_H
#define CS4023_ARRAYS_H
///Structs and enums
typedef enum{
    success,
    noMemory,
    illegalNode
} array_errorCode;

typedef struct n{
    int number;
    struct n *successor;
} int_array_node;

typedef struct{
    int_array_node *head;
    int_array_node *current;
    int size;
} int_array;

///Function calls
int_array *int_createArray();
void int_goToHead(int_array *array);
array_errorCode int_goToNext(int_array *array);
void int_deleteArray(int_array *array);

#endif //CS4023_ARRAYS_H
